package fr.ulille.iut.tva.ressource;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

public class NiveauTvaInexistantException extends WebApplicationException {
	private static final long serialVersionUID = 939875418210403804L;

	public NiveauTvaInexistantException() {
        super(Response.status(Response.Status.NOT_ACCEPTABLE).entity("Niveau de TVA inexistant\n").build());
    }
}
